# -*- coding: UTF-8 -*-
"""
Author: Jaime Rivera
File: config_utils.py
Date: 2019.11.30
Revision: 2020.01.12
Copyright: Copyright 2019 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief:

"""

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2019, Jaime Rivera'
__credits__ = ['The "Enjoying Code" group at Ilion Animation Studios']
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Development'

import os
import getpass

import yaml


YAML_FILE = os.path.dirname(os.path.abspath(__file__)).replace('\\', '/') + '/particle_simulations.yml'

def get_number_of_simulations():
    with open(YAML_FILE, 'r') as y:
        config_dict = yaml.load(y, Loader=yaml.FullLoader)
        return len(config_dict.keys())

def get_simulation_parameter(sim_index, param):
    with open(YAML_FILE, 'r') as y:
        config_dict = yaml.load(y, Loader=yaml.FullLoader)
        return config_dict['SIMULATION {}'.format(sim_index)][param]

def get_particle_parameters(sim_index):
    with open(YAML_FILE, 'r') as y:
        config_dict = yaml.load(y, Loader=yaml.FullLoader)
        return config_dict['SIMULATION {}'.format(sim_index)]['PARTICLE PROPERTIES']

def generate_output_feedback(sim_index):

    with open(YAML_FILE, 'r') as y:
        config_dict_sim = yaml.load(y, Loader=yaml.FullLoader)['SIMULATION {}'.format(sim_index)]

    output_path = config_dict_sim['output_path'].format(getpass.getuser())


    output_text = "Initial count: {total_particles} particles - Radius: {particle_radius}\n" \
                  "Rotation randomisation: {speed_rot_randomisation_factor}" \
                  "- Min. speed: {min_speed_mult} - Max. speed: {max_speed_mult}\n" \
                  "".format(**config_dict_sim, **config_dict_sim['PARTICLE PROPERTIES'])

    output_text += "Neutral: {}% - Rebound: {}% - Destroy: {}% - Fusion: {}%\n" \
                   "".format(*config_dict_sim['PARTICLE PROPERTIES']['collision_probabilities'])

    output_text += "TRI: {}% - SQU: {}% - PEN: {}% - STAR: {}% - CIR: {}%\n" \
                   "".format(*config_dict_sim['PARTICLE PROPERTIES']['polygon_probabilities'])

    output_text +=  "Trails: {paint_trails} - Min.length: {min_trail_length} - Max. lenght: {max_trail_length}" \
                   "".format(**config_dict_sim['PARTICLE PROPERTIES'])


    info_txt = output_path + 'SIMULATION_{}/sim_info.txt'.format(sim_index)
    with open(info_txt, 'w') as t:
        t.write(output_text)
