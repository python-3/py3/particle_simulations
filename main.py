# -*- coding: UTF-8 -*-
"""
Author: Jaime Rivera
File: main.py
Date: 2019.11.29
Revision: 2020.01.12
Copyright: Copyright 2019 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief:

"""

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2019, Jaime Rivera'
__credits__ = ['The "Enjoying Code" group at Ilion Animation Studios']
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Development'


import sys
import os
import getpass
import time
import logging

from PIL import Image
from PIL import ImageDraw

import config_utils
import particle_classes


logger = logging.getLogger()


# ------------- PAINT METHOGS ------------- #
def paint_particle_trail(particle, image):
    if not particle.paint_trails:
        return

    draw = ImageDraw.Draw(image)
    color = particle.color

    for i in range(1, len(particle.trails)):
        tx1, ty1 = particle.trails[i].x, particle.trails[i].y
        tx2, ty2 = particle.trails[i - 1].x,  particle.trails[i - 1].y

        idx = 0.5 * (1.0 - float(i) / len(particle.trails))
        faded_color = (int(color[0] * idx), int(color[1] * idx), int(color[2] * idx))
        draw.line([tx1, ty1, tx2, ty2, ], fill=faded_color)

def paint_particle_head(particle, image):
    pixels = image.load()
    draw = ImageDraw.Draw(image)

    # Painting polygon
    if particle.get_associated_poly() != particle_classes.CIRCLE:
        draw.polygon(particle.get_shape_coords(), outline='rgb' + str(particle.color),
                     fill='rgb' + str(particle.darker_color))
    else:
        draw.ellipse(particle.get_shape_coords(), outline='rgb' + str(particle.color),
                     fill='rgb' + str(particle.darker_color))


# ------------- RUN A SIMULATION ------------- #
def run(sim_index):
    # Output parameters
    output_path = config_utils.get_simulation_parameter(sim_index, 'output_path').format(getpass.getuser())
    res = config_utils.get_simulation_parameter(sim_index, 'res')
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    # Simulation parameters
    simulation_start = config_utils.get_simulation_parameter(sim_index, 'simulation_start')

    first_output_frame = config_utils.get_simulation_parameter(sim_index, 'first_output_frame')
    first_collisions_frame = config_utils.get_simulation_parameter(sim_index, 'first_collisions_frame')
    simulation_end = config_utils.get_simulation_parameter(sim_index, 'simulation_end')

    # Particle count
    total_particles = config_utils.get_simulation_parameter(sim_index, 'total_particles')

    # Simulation output
    sim_out = output_path + 'SIMULATION_{}/'.format(sim_index)
    if not os.path.exists(sim_out):
        os.makedirs(sim_out)

    # SIMULATION
    collisions_active = False

    particles = []
    for i in range(total_particles):
        kwargs_dict = config_utils.get_particle_parameters(sim_index)
        kwargs_dict['boundaries'] = res
        new_particle = particle_classes.MovingParticle(**kwargs_dict)
        particles.append(new_particle)

    for t in range(simulation_start, simulation_end):
        logger.info('Simulation {}, CALCULATING FRAME {}'.format(sim_index, t))
        img = Image.new('RGB', res, color=(0, 0, 0))

        # Moving and painting
        for p in particles:
            p.move()
            paint_particle_trail(p, img)
        for p in particles:
            paint_particle_head(p, img)

        # Collisions
        for p in particles:
            p.to_collide_now.clear()
            point_tuple = p.get_current_position()
            if not collisions_active:
                continue
            for other_particle in particles:
                if p == other_particle:
                    continue
                collision = p.collide_with_particle(other_particle)
                if collision is None:
                    continue
                logger.debug('HIT! AT FRAME {} OF TYPE {}'.format(t, collision))
                if collision in [particle_classes.DESTROY_COLL, particle_classes.FUSION_COLL]:
                    if p in particles:
                        particles.remove(p)
                    if other_particle in particles:
                        particles.remove(other_particle)
                    if collision == particle_classes.FUSION_COLL:
                        kwargs_dict = config_utils.get_particle_parameters(sim_index)
                        kwargs_dict['boundaries'] = res
                        kwargs_dict['initial_pos'] = point_tuple
                        kwargs_dict['poly'] = particle_classes.CIRCLE
                        particles.append(particle_classes.MovingParticle(**kwargs_dict))

        for p in particles:
            p.solve_rebound_collisions()

        # Start the collisions
        if t >= first_output_frame:
            out_image = sim_out + 'particles.###.png'
            img.save(out_image.replace('###', str(t).zfill(3)))
            if t > first_collisions_frame:
                collisions_active = True


# ------------- MAIN ------------- #
def main(argv=None):
    if argv is None:
        argv = sys.argv

    logger.info("---SIMULATION STARTED---")
    logger.info("Number of simulations tu run: {}".format(config_utils.get_number_of_simulations()))

    for i in range(config_utils.get_number_of_simulations()):
        run(i + 1)
        config_utils.generate_output_feedback(i + 1)

    return 0


# ------------- EXECUTION ------------- #
if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s|%(levelname)s|%(message)s',
        )
    start_time = time.time()
    try:
        sys.exit(main())
    except Exception:
        logging.exception("Unhandled exception running process")
    finally:
        logger.info("Process took {:.2f}secs".format(time.time()-start_time))